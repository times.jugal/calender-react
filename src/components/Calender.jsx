import React, { useEffect, useState } from 'react'
import './calender.css'
import moment from 'moment/moment';

const Calender = ({ date }) => {
    const [dateObject, setDateObject] = useState(moment())
    const weekdayshort = moment.weekdaysMin();

    /**
     * Array of week days (Mo, Tu, We...)
     * returns JSX 
     */
    let weekdayshortname = weekdayshort.map(day => {
        return <th key={day}>{day}</th>;
    });

    useEffect(() => {
        // Initialise state object
        if (date) {
            const _date = moment(date)
            setDateObject(moment(_date))
        }
    }, [date])

    const firstDayOfMonth = () => {
        const firstDay = moment(dateObject)
            .startOf("month")
            .format("d");
        return firstDay;
    };

    /**
     * Getter fn to return current day of the dateObject
     * @returns String
     */
    const currentDay = () => {
        return dateObject.format("D");
    };

    /**
     * Returns day index {Su: '0', Mo: '1'...}
     * @returns String
     */
    const getDaysInMonth = () => {
        return moment(dateObject).daysInMonth();
    }

    /**
     * Insert blanks till the first day {Mo, Tu, We...} of the month
     */
    let blanks = [];
    for (let i = 0; i < firstDayOfMonth(); i++) {
        blanks.push(
            <td key={'blank' + i} className="calendar-day empty">{""}</td>
        );
    }

    /**
     * Insert all the dates for the month. {For e.g May 31 days, June 30 days}
     */
    let numOfDaysInMonth = [];
    for (let d = 1; d <= getDaysInMonth(); d++) {
        let today = (d.toString() === currentDay()) ? "today" : "";
        numOfDaysInMonth.push(
            <td key={d} className={"calendar-day " + today}>
                {d}
            </td>
        );
    }

    /**
     * Create table rows after each week ends and cells for each day in the week
     */
    const totalSlots = [...blanks, ...numOfDaysInMonth];
    const rows = [];
    let cells = [];
    totalSlots.forEach((row, i) => {
        if (i % 7 !== 0) {
            cells.push(row);
        } else {
            rows.push(cells);
            cells = [];
            cells.push(row);
        }
        if (i === totalSlots.length - 1) {
            rows.push(cells);
        }
    });

    /**
     * Create JSX of tr for every rows
     */
    const _daysinmonth = rows.map((d, i) => {
        return <tr key={i}>{d}</tr>;
    });

    return (
        <>
            <div className="calendar-date">
                <div className="calendar-month-year">
                    <span>{moment(dateObject).format("MMMM")}</span>
                    <span>{moment(dateObject).format('YYYY')}</span>
                </div>
                <table className="calendar-day">
                    <thead>
                        <tr>{weekdayshortname}</tr>
                    </thead>
                    <tbody>{_daysinmonth}</tbody>
                </table>
            </div>
        </>
    )
}

export default Calender