import './App.css';
import Calender from './components/Calender';

function App() {
  return (
    <div className="App">
      <Calender date={'2023-06-06'} />
    </div>
  );
}

export default App;
